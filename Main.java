package Failiki;

import java.util.Scanner;

public  class Main {
    public static void main(String[] args) {
        String s = new Scanner(System.in).nextLine();

        if (s.length() > 0) {
            Zad(s, s.length()-1);
        }
    }
    private static void Zad(String s, int index) {
        if (index == 0 ) {
            System.out.println(s.charAt(index));
            return;
        }
        System.out.print(s.charAt(index));

        Zad(s, index-1);
    }
}
